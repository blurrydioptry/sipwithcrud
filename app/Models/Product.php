<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    //kolom apa saja yang boleh diisi
    // protected $fillable =[]
    //kolom apa saja yang tidak boleh diisi
    protected $guarded = [
    'id',
    'created_at',
    'updated_at',
    ];

    public function getCategory()
    {
        return $this->belongsTo(category::class, 'category_id', 'id');
    }

}
