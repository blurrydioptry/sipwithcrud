@extends('admin/admin')
@section('title', 'Product')
@section('content-title')
<div class="row mb-2">
    <div class="col-sm-6">
      <h1 class="m-0">Product</h1>
    </div><!-- /.col -->
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item active">Product</li>
      </ol>
    </div>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col">

    {{ Form::open(['route' => ['product.update', $product->id], 'method' => 'put','files' => true]) }}
      <div class="card">
          <div class="card-header">
              <h3 class="card-title">Update Product</h3>
          </div>
          <div class="card-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                   <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                   </ul>
                </div>
            @endif

            @if (!empty($product->image))
            <img src="{{asset('storage/product/' . $product->image)}}" height="100" alt="">
            @endif

            <div class="row">
                <div class="col">
                    <div class="form-group">
                        {{form::label('category_id', 'Category') }}
                        {{form::select ('category_id', $categories, $product->category_id, ['class'=>'form-control', 'placeholder' => 'choose category'])}}

                    </div>
                    <div class="form-group">
                    {{form::label('name','Name')}}
                    {{form::text('name', $product->name,['class'=>'form-control', 'placeholder' => 'enter name'])}}
                </div>
                <div class="form-group">
                {{form::label('price','Price')}}
                {{form::number('price', $product->price,['class'=>'form-control', 'placeholder' => 'enter Price'])}}
                </div>
            </div>
                <div class="col">
                    <div class="form-group">
                        {{ form::label('sku','SKU')}}
                        {{ form::text('sku', $product->sku,['class'=>'form-control', 'placeholder' => 'enter sku'])}}
                    </div>
                    <div class="form-group">
                        {{ form::label('status', 'Status') }}
                        {{form::select ('status', ['active' => 'Active','inactive'=>'inactive'], null, ['class'=>'form-control', 'placeholder' => 'choose status'])}}
                    </div>
                    <div class="form-group">
                        {{ form::label('image') }}
                        {{ form::file('image',['class'=>'form-control']) }}
                    </div>
                </div>
            </div>

            <div class="form-group">
                {{form::label('description') }}
                {{form::textarea ('description', null, ['class'=>'form-control', 'placeholder' => 'enter description', 'rows' => 4])}}
            </div>

    </div>
            <div class="card-footer">
              <a href="{{ url('admin/category')}}" class="btn btn-outline-info">Back</a>
              <button type="submit" class="btn btn-primary float-right">Update Product</button>
            </div>
    </div>
    {{ form::close() }}

    </div>
</div>
</div>
@endsection
